#!/bin/bash

#valgrind --memcheck:leak-check=yes --show-reachable=yes ./pron_eval e_call.pron.cfg 

valgrind --tool=memcheck --leak-check=full --show-leak-kinds=all --suppressions=valgrind-cuda-supp.txt ./pron_eval e_call.pron.cfg
