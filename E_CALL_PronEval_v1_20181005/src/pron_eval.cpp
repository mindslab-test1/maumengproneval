

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <time.h>
#include <pthread.h>
#include <sys/time.h>
#include <unistd.h>

#include "Laser.h"
#include "confmeasure.h"
#include "pronClinic.h"
#include "result.h"

#define	NUM_THREAD	1
#define	MAXCHAR	1024
#define MAXLENDATA 9999

void* ASR_SVC_THREAD(void *data)
{
	void	*pronStructM = NULL;
	void 	*pronStructC = NULL;
	char	*pronResult;
	char	data_s2c[MAXLENDATA*128];
	DECODER_RESULT *result = NULL;
	SESSIONINFO ssinfo;

	pronResult = (char*)malloc(sizeof(char)*MAXCHAR*12);
	strcpy(pronResult,"");

	result = (DECODER_RESULT*) malloc( sizeof( DECODER_RESULT ) );
	memset(result,0,sizeof(DECODER_RESULT));

	strcpy(result->m_result,"i went hiking with my father");

	pronStructM = (void*)data;

	pronStructC = createPronScoreDnnChild( pronStructM );

	doPronScore( pronStructC, result  , "sample.pcm"  , pronResult);	// teset file : sample.pcm

	strcpy(ssinfo.m_userid,"test");
	strcpy(ssinfo.m_lessonid,"lessonXXX");
	strcpy(ssinfo.m_instanceid,"instanceXXX");
	strcpy(ssinfo.m_scripttext,result->m_result);

	makeXmlResultDataRolePlay(data_s2c, result->m_result, 0, 0, result, &ssinfo, pronResult);
	fprintf(stderr,"%s\n", data_s2c);

	freePronScore( pronStructC );

	free( pronResult );
	free( result );

	return NULL;
}

int main(int argc, char *argv[])
{
	int				nt, n_thread;
	pthread_t		p_thr[NUM_THREAD];
	void*			pronStruct;

	if( argc != 2 )
	{
		fprintf( stderr, "USAGE: %s pron.cfg\n", argv[0]);
		return -1;
	}

	pronStruct = createPronScoreDnnMaster(argv[1]);

	n_thread = NUM_THREAD;
    for ( nt=0; nt<n_thread; nt++ ) {
        if (pthread_create(&p_thr[nt], NULL, ASR_SVC_THREAD, (void *)pronStruct) < 0) {
			fprintf(stderr,"thread Create error : no available resources\n");
			exit(0);
		}
		usleep( 0 );
	}

	for ( nt=0; nt<n_thread; nt++ ) {
		(void) pthread_join(p_thr[nt], NULL);
	}

	freePronScoreDnnMaster( pronStruct );

	return 0;
}
