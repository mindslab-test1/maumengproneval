#ifndef _RESULT_H_
#define _RESULT_H_

//#define MAX_NUM_MASTERS 5
#define MAX_LEN_SENTSTRING  2048 

typedef struct _SESSIONINFO
{
	char m_userid[MAX_LEN_SENTSTRING];
	char m_lessonid[MAX_LEN_SENTSTRING];
	char m_instanceid[MAX_LEN_SENTSTRING];
	char m_utteranceid[MAX_LEN_SENTSTRING];
	char m_tasktype[MAX_LEN_SENTSTRING];
	char m_stepname[MAX_LEN_SENTSTRING];
	char m_scripttext[MAX_LEN_SENTSTRING];
	char m_clinictype[MAX_LEN_SENTSTRING];
	char m_usersex[MAX_LEN_SENTSTRING];
	char m_nativesex[MAX_LEN_SENTSTRING];
	char m_nativefname[MAX_LEN_SENTSTRING];
	char m_deviceinfo[MAX_LEN_SENTSTRING];
	char m_protocolVer[MAX_LEN_SENTSTRING];
	int m_dialogturn;
	int m_istarget;
	int m_windowWidth;
	int m_dialogOpen;
} SESSIONINFO;

int makeXmlResultDataRolePlay(char* data_s2c, char* postproc_result, int start_idx, int end_idx, DECODER_RESULT* result, SESSIONINFO *ssinfo, char* result_clinic);

#endif
