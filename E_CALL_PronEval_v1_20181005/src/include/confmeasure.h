#ifndef _CONFMEASURE_H_
#define _CONFMEASURE_H_

#ifdef _USE_PTHREAD_MUTEX_
#include <pthread.h>
#endif

#include <stdio.h>

#ifdef __cplusplus
extern "C"{
#endif

struct _VocabSet;
struct _pRuleSet;

#define APITYPE 

typedef struct _CONFMEASURE
{
    void* m_arec;
    void** m_agset;
    void* m_gbuilder;
    struct _VocabSet* m_vs;
    void* m_uv;

    struct _pRuleSet* m_ruleA;
    unsigned int m_opt;

    float m_sentthresh;
#ifdef _USE_PTHREAD_MUTEX_
    pthread_mutex_t m_mutex;
#endif
} CONFMEASURE;

#define MAX_LEN_RESULTSTRING    4096
#define MAX_NUM_WORD 256 
#define MAX_NUM_PH 1024
#define MAX_LEN_WORDSTRING  64
#define	MAX_INT_SAMPLE 1024
#define MAX_LEN_FNAME   2048

typedef struct _DECODER_RESULT
{
    char m_result[MAX_LEN_RESULTSTRING];

    float m_confidence;
    float m_confidenceA[MAX_NUM_WORD];
    char  m_wordnameA[MAX_NUM_WORD][MAX_LEN_WORDSTRING];
    int m_startA[MAX_NUM_WORD];
    int m_endA[MAX_NUM_WORD];
    int m_numword;

    int m_pronscore;
    int m_pronscoreA[MAX_NUM_WORD];

	int m_stressA[MAX_NUM_WORD];
	int	m_stressscore;

    char  m_phnameA[MAX_NUM_PH][MAX_LEN_WORDSTRING];
    int m_phendA[MAX_NUM_PH];
    int m_phscoreA[MAX_NUM_PH];
    int m_numph;

    float m_intonation[MAX_INT_SAMPLE];
	int	m_intonationLen;
	int m_intonationscore;

	char m_logfname[MAX_LEN_FNAME];
} DECODER_RESULT;


APITYPE CONFMEASURE* createConfMeasure(
        const char* am, const char* fsm, const char* sym, const char* isym,
        const char* aam, const char* agmm,
        const char* pset, const char* cset,
        const char* voc, const char* pronrule, const char* beam );
APITYPE void freeConfMeasure( CONFMEASURE* cm );
APITYPE int resetConfMeasure();
APITYPE int doConfMeasure( CONFMEASURE* cm, unsigned int ilen, float* feat, DECODER_RESULT *result);

#ifdef __cplusplus
}
#endif
#endif
