#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <time.h>
#include <pthread.h>
#include <sys/time.h>
#include <unistd.h>

#include "confmeasure.h"
#include "result.h"

#define INTONATION_MAX 100
#define MAXWORD 128

int stringTokenize( char *str, char** wordA, char t_mask)
{
    char    *ptr;
    int     n_tok = 0;

    while( *str)
    {
        if( *str == '\0') break;

        ptr = strchr( str, t_mask);
        if( ptr )
        {
            unsigned int len = ptr - str;
            strncpy( wordA[n_tok], str, len );
            wordA[n_tok][len] = '\0';
            n_tok ++;
        }
        else
        {
            strcpy( wordA[n_tok], str);
            n_tok ++;
            break;
        }

        str = ptr + 1;
    }

    return n_tok;
}

void parsingClinicResult( char* result_clinic, char* result_clinic_parsing)
{
    char *p;

    p = strstr(result_clinic,"<PronClinic>");
    strcpy(result_clinic_parsing, p);
}


int makeXmlResultDataASRBody(char* data_s2c, char* postproc_result, int start_idx, int end_idx, DECODER_RESULT* result, SESSIONINFO *ssinfo)
{
    strcat(data_s2c,"       <asr>\n");
    strcat(data_s2c,"           <sentResult>\n");
    sprintf(data_s2c,"%s                <result>%s</result>\n",data_s2c,postproc_result);
    sprintf(data_s2c,"%s                <startTime>%.2f</startTime>\n",data_s2c,(float)start_idx/100.0);
    sprintf(data_s2c,"%s                <endTime>%.2f</endTime>\n",data_s2c,(float)end_idx/100.0);
    sprintf(data_s2c,"%s                <uvScore>%.2f</uvScore>\n",data_s2c,result->m_confidence);
    sprintf(data_s2c,"%s                <pronScore>%d</pronScore>\n",data_s2c,result->m_pronscore);
    strcat(data_s2c,"           </sentResult>\n");

    {
        strcat(data_s2c,"           <wordResult>\n");
        for(int i=0;i<result->m_numword;i++)
        {
            if(i > 0 && result->m_endA[i-1] < result->m_startA[i]-1)
            {
                strcat(data_s2c,"                <word>\n");
                sprintf(data_s2c,"%s                    <name>%s</name>\n",data_s2c,"sil");
                sprintf(data_s2c,"%s                    <startTime>%.2f</startTime>\n",data_s2c,(float)result->m_endA[i-1]/100.0+0.01);
                sprintf(data_s2c,"%s                    <endTime>%.2f</endTime>\n",data_s2c,(float)result->m_startA[i]/100.0-0.01);
                sprintf(data_s2c,"%s                    <uvScore>%.2f</uvScore>\n",data_s2c,0.0);
                sprintf(data_s2c,"%s                    <pronScore>%d</pronScore>\n",data_s2c,0);
                sprintf(data_s2c,"%s                    <stressValue>%d</stressValue>\n",data_s2c,0);
                strcat(data_s2c,"                </word>\n");
            }

            strcat(data_s2c,"               <word>\n");
            if(strstr(result->m_wordnameA[i],"<s>") || strstr(result->m_wordnameA[i],"</s>"))
                sprintf(data_s2c,"%s                    <name>%s</name>\n",data_s2c,"sil");
            else
                sprintf(data_s2c,"%s                    <name>%s</name>\n",data_s2c,result->m_wordnameA[i]);
            sprintf(data_s2c,"%s                    <startTime>%.2f</startTime>\n",data_s2c,(float)result->m_startA[i]/100.0);
            sprintf(data_s2c,"%s                    <endTime>%.2f</endTime>\n",data_s2c,(float)result->m_endA[i]/100.0);
            sprintf(data_s2c,"%s                    <uvScore>%.2f</uvScore>\n",data_s2c,(float)result->m_confidenceA[i]);
            sprintf(data_s2c,"%s                    <pronScore>%d</pronScore>\n",data_s2c,result->m_pronscoreA[i]);
            sprintf(data_s2c,"%s                    <stressValue>%d</stressValue>\n",data_s2c,(int)result->m_stressA[i]);
            strcat(data_s2c,"               </word>\n");

			printf("%s -> %d\n", result->m_wordnameA[i], (int)result->m_stressA[i]);

        }
        strcat(data_s2c,"           </wordResult>\n");
    }
    if(result->m_intonationLen > 0)
    {
        strcat(data_s2c,"           <intonation>\n");
        strcat(data_s2c,"               <valueMin>0</valueMin>\n");
        sprintf(data_s2c,"%s                <valueMax>%d</valueMax>\n",data_s2c,INTONATION_MAX);
        sprintf(data_s2c,"%s                <size>%d</size>\n",data_s2c,result->m_intonationLen);
        strcat(data_s2c,"               <data>");
        for(int i=0; i<result->m_intonationLen; i++)
            sprintf(data_s2c,"%s%d ",data_s2c,(int)result->m_intonation[i]);
        strcat(data_s2c,"</data>\n");
        strcat(data_s2c,"           </intonation>\n");
    }
    sprintf(data_s2c,"%s            <logfname>\"%s\"</logfname>\n",data_s2c,result->m_logfname);
    strcat(data_s2c,"       </asr>\n");
    return 1;
}

int makeXmlResultDataRolePlay(char* data_s2c, char* postproc_result, int start_idx, int end_idx, DECODER_RESULT* result, SESSIONINFO *ssinfo, char* result_clinic)
{
    int i, nFlds;
    char **ctmpA;
    char *result_clinic_parsing;

    ctmpA = (char**)malloc(sizeof(char*) * MAXWORD);
    for(i=0;i<MAXWORD; i++)
        ctmpA[i] = (char*)malloc(sizeof(char)*128);

    result_clinic_parsing = (char*)malloc(sizeof(char) * strlen(result_clinic) + 12);

    nFlds = stringTokenize(ssinfo->m_scripttext, ctmpA, ' ');
    for(i=0;i<result->m_numword;i++)
    {
        if(i==0)
        {
            strcpy(result->m_wordnameA[i], "<s>");
        }
        else if(i== result->m_numword-1)
        {
            strcpy(result->m_wordnameA[i], "</s>");
        }
        else
        {
            strcpy(result->m_wordnameA[i], ctmpA[i-1]);
        }
    }

    strcpy(data_s2c,"<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n");
    strcat(data_s2c,"<wid>\n");
    strcat(data_s2c,"   <header>\n");
    strcat(data_s2c,"       <title>server to client data (Role-Play)</title>\n");
    strcat(data_s2c,"   </header>\n");
    strcat(data_s2c,"   <body>\n");

    strcat(data_s2c,"       <RolePlay>\n");
    sprintf(data_s2c,"%s            <LessonId>%s</LessonId>\n",data_s2c,ssinfo->m_lessonid);
    sprintf(data_s2c,"%s            <InstanceId>%s</InstanceId>\n",data_s2c,ssinfo->m_instanceid);
    sprintf(data_s2c,"%s            <SentenceText>%s</SentenceText>\n",data_s2c,ssinfo->m_scripttext);
    sprintf(data_s2c,"%s            <dialogTurn>%d</dialogTurn>\n",data_s2c,ssinfo->m_dialogturn);
    strcat(data_s2c,"       </RolePlay>\n");

    makeXmlResultDataASRBody(data_s2c,postproc_result, start_idx, end_idx, result, ssinfo );

    if(strcmp(result_clinic,"") != 0){
        parsingClinicResult( result_clinic, result_clinic_parsing);
        sprintf(data_s2c,"%s        %s",data_s2c,result_clinic_parsing);
    }

    strcat(data_s2c,"       <engineVer>gt v2.3</engineVer>\n");
    strcat(data_s2c,"   </body>\n");
    strcat(data_s2c,"</wid>\n");

    for(i=0;i<MAXWORD; i++)
        free(ctmpA[i]);
    free(ctmpA);

    free(result_clinic_parsing);

    return 1;
}
