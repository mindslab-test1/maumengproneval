#include <iostream>
#include <stdlib.h>
#include <unistd.h>
/*
#include <libconfig.h++>
*/
#include <iostream>
#include <memory>
#include <string>
#include <poll.h>

#include "mslPronEval_test.h"
#include "option.h"
#include "mslPronEval_Client.h"

/* ########################################################################################## */

env_t g_env;

/* ########################################################################################## */

/* */
int main(int argc, char *argv[]) 
{
	setOptions(argc, argv);

	printf("======================================================\n");
	printf("PronEval Test \n");
	printf("------------------------------------------------------\n");
	printf("host = %s\n", g_env.host.c_str());
	printf("filename = %s\n", g_env.filename.c_str());
	printf("answer_text = %s\n", g_env.answer_text.c_str());
	printf("------------------------------------------------------\n");
	printf("\n\n");

	mslPronEval_Client client(grpc::CreateChannel(g_env.host, grpc::InsecureChannelCredentials()));
	Response_EnglishPronEval *resp = client.evaluateEnglishPron(g_env.filename, g_env.answer_text);
	if(resp == NULL) {
		printf("@error: grpc fail\n");
	}
	else {
		printf("@info: grpc succ\n");
		printf("       result_code = %d\n", resp->result_code());       
		printf("       trans_id = %s\n", resp->trans_id().c_str());       

		printf("       sentence = %s\n", resp->sentence().c_str());       

		printf("       confidence = %f\n", resp->confidence());       
		printf("       pron_score = %d\n", resp->pron_score());       
		printf("       stress_score = %d\n", resp->stress_score());       

		printf("       holistic = %f\n", resp->regression_holistic());       
		printf("       speed = %f\n", resp->regression_speed());       
		printf("       rhythm = %f\n", resp->regression_rhythm());       
		printf("       intonation = %f\n", resp->regression_intonation());       
		printf("       segmental = %f\n", resp->regression_segmental());       
		printf("       segmental_feat19 = %f\n", resp->regression_segmental_feat19());       

		for(int xx = 0; xx < resp->list_word_size(); xx++) {
			printf("       word {\n");
			printf("               name = %s \n", resp->list_word(xx).name().c_str());       
			printf("               start_time = %d \n", resp->list_word(xx).start_time());       
			printf("               end_time = %d \n", resp->list_word(xx).end_time());       
			printf("               confidence = %f \n", resp->list_word(xx).confidence());       
			printf("               pron_score = %d \n", resp->list_word(xx).pron_score());       
			printf("               stress = %d \n", resp->list_word(xx).stress());       
			printf("       }\n");       
		}

		for(int xx = 0; xx < resp->list_phonics_size(); xx++) {
            printf("       phonics {\n");
            printf("               name = %s \n", resp->list_phonics(xx).name().c_str());
            printf("               end_time = %d \n", resp->list_phonics(xx).end_time());
            printf("               score = %d \n", resp->list_phonics(xx).score());
            printf("       }\n");
        }


	}

	printf("\n\n");
	return 0;
}
