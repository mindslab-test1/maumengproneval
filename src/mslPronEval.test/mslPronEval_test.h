#ifndef __mslPronEval_test_h__
#define __mslPronEval_test_h__

#include <iostream>
#include <string>

using namespace std;

/* ########################################################################################## */

typedef struct {
	string 			host;
	string			filename;
	string			answer_text;
} env_t;

/* ########################################################################################## */

extern env_t g_env;

/* ########################################################################################## */

#endif
