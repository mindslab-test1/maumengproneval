#ifndef __mslPronEval_Client_h__
#define __mslPronEval_Client_h__

#include <iostream>
#include <memory>
#include <string>

#include <grpcpp/grpcpp.h>
#include <grpc/support/log.h>

#include "mslPronEval.grpc.pb.h"

using namespace std;

using grpc::Channel;
using grpc::ClientAsyncResponseReader;
using grpc::ClientContext;
using grpc::CompletionQueue;
using grpc::Status;

using ai::mindslab::eduai::pron_eval::Request_EnglishPronEval;
using ai::mindslab::eduai::pron_eval::Response_EnglishPronEval;
using ai::mindslab::eduai::pron_eval::Service_EnglishPronEval;

class mslPronEval_Client {
public:
	explicit mslPronEval_Client(std::shared_ptr<Channel> channel) : stub_(Service_EnglishPronEval::NewStub(channel)) {}

	/* 평가 요청 */
	Response_EnglishPronEval *
	evaluateEnglishPron(std::string &filename, std::string &answer_text);

private:
	std::unique_ptr<Service_EnglishPronEval::Stub> stub_;

};

#endif
