#include "mslPronEval_Client.h"

Response_EnglishPronEval *
mslPronEval_Client::evaluateEnglishPron(string &filename, string &utterance) 
{
	// SET - REQUEST
	Request_EnglishPronEval 	request;
	request.set_utterance(utterance);
	request.set_filename(filename);
	request.set_trans_id("test.100");

	// RESPONSE
	Response_EnglishPronEval *	response = new Response_EnglishPronEval;	

	// CONTEXT
	ClientContext				context;

	// QUEUE FOR ASYNC
	CompletionQueue cq;

	// GRPC STATUS
	Status status;

	// GRPC OBJECT
	std::unique_ptr<ClientAsyncResponseReader<Response_EnglishPronEval> > 
								rpc(stub_->PrepareAsync_evaluateEnglishPron(&context, request, &cq));

	// RPC CALL
	rpc->StartCall();

	// RPC FINISH
	rpc->Finish(response, &status, (void*) 1);

	// STATUS
	void* got_tag = NULL;
	bool ok = false;

	cq.Next(&got_tag, &ok);

	if(status.ok()) return response;
	else {
		if(response != NULL) delete response;
		return NULL;
	}
}
