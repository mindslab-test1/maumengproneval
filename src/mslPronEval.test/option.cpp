#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>
#include "mslPronEval_test.h"

using namespace std;

/* ##################################################################################### */

void printUsage() ;

/* ##################################################################################### */

#define DEFAULT_HOST				"127.0.0.1:15000"
#define DEFAULT_ANSWER_TEXT			"I went hiking with my father."
#define DEFAULT_FILENAME			"/home/minds/project/maumEngPronEval/src/mslPronEval.test/sample/sample.pcm"

/* ##################################################################################### */

typedef void (*optCallback_fn)(void *aaa, void *arg_val);

typedef enum {
	mslOPT_ARG___NONE = 0,
	mslOPT_ARG___STRING = 1,
	mslOPT_ARG___INT = 2,
} mslOption_arg_e;


typedef struct {
	const char *		name;
	mslOption_arg_e		arg_type;
	void *				target;
	const void *		default_val;
	optCallback_fn		callback;
} mslOption_t;

/* ##################################################################################### */

int procOption(int cur_idx, int total, char *argv[], mslOption_t *opts) ;
bool isAvailableOptionValue(int cur_idx, int total);
void optCallback_Usage(void *aa, void *arg_val);
void setDefault(mslOption_t *opts);
void setValue(mslOption_arg_e type, void *target, const void *value);
void printUsage();

/* ##################################################################################### */

int setOptions(int argc, char *argv[]) 
{
	mslOption_t opts[] = {
		{"-h", 		mslOPT_ARG___NONE , 	NULL, 					NULL, 					optCallback_Usage	},
		{"-host", 	mslOPT_ARG___STRING, 	&g_env.host, 			DEFAULT_HOST, 			NULL, 				},
		{"-a", 		mslOPT_ARG___STRING, 	&g_env.answer_text, 	DEFAULT_ANSWER_TEXT, 	NULL, 				},
		{"-f", 		mslOPT_ARG___STRING, 	&g_env.filename, 		DEFAULT_FILENAME, 		NULL, 				},
		{NULL,  	mslOPT_ARG___NONE, 		NULL, 					NULL, 					NULL, 				}
	};
	int arg_index = 0;

	/* 디폴트값 설정 */
	setDefault(opts);

	/* 인자가 없으면 기본값으로 실행한다. */
	if(argc == 1) return 0;

	for(arg_index = 1; arg_index < argc; ) {
		arg_index = procOption(arg_index, argc, argv, opts); 		

		if(arg_index < 0) optCallback_Usage(NULL, NULL);
	}
}

/*
 * 옵션 파라메타 개별 처리
 */
int procOption(int cur_idx, int total, char *argv[], mslOption_t *opts) 
{
	if(cur_idx >= total) return total;

	// printf("@debug: cur arg >> %s\n", argv[cur_idx]);
	for(int xx = 0; opts[xx].name != NULL; xx++) {
		if(strcmp(argv[cur_idx], opts[xx].name) == 0) {
			if(opts[xx].arg_type == mslOPT_ARG___NONE) {
				if(opts[xx].callback != NULL) opts[xx].callback(&opts[xx], NULL);
				cur_idx++;
			}
			else if(opts[xx].arg_type == mslOPT_ARG___STRING) {
				// 값으로 사용 항목이 존재하는지 확인
				if(isAvailableOptionValue(cur_idx, total) == false) {
					printf("@error: option >> missing arg value\n");
					return -1;
				}

				// 값 설정 
				setValue(opts[xx].arg_type, opts[xx].target, argv[cur_idx+1]);

				if(opts[xx].callback != NULL) opts[xx].callback(&opts[xx], NULL);
				cur_idx += 2;
				return cur_idx;
			}
			else if(opts[xx].arg_type == mslOPT_ARG___INT) {
				// 값으로 사용 항목이 존재하는지 확인
				if(isAvailableOptionValue(cur_idx, total) == false) {
					printf("@error: option >> missing arg value\n");
					return -1;
				}
				
				// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
				// 차후에 정수형 체크 기능 추가
				// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
				
				// 값 설정 
				setValue(opts[xx].arg_type, opts[xx].target, argv[cur_idx+1]);
				
				if(opts[xx].callback != NULL) opts[xx].callback(&opts[xx], NULL);
				cur_idx += 2;
				return cur_idx;
			}
		}
	}

	printf("@error: option >> not found\n");
	return -1;
}

/*
 * Option Value로 사용할 파라메타가 존재하는지 확인
 * 		==> exec arg arg_val
 *			cur_idx = 1
 *			total = 3
 */
bool isAvailableOptionValue(int cur_idx, int total) 
{
	if(cur_idx >= total -1) return false;
	return true;
}


void setDefault(mslOption_t *opts)
{
	for(int xx = 0; opts[xx].name != NULL; xx++) {
		if(opts[xx].default_val == NULL) continue;
		if(opts[xx].target == NULL) continue;

		setValue(opts[xx].arg_type, opts[xx].target, opts[xx].default_val);
	}
}

void setValue(mslOption_arg_e type, void *target, const void *value)
{
	if(type == mslOPT_ARG___STRING) {
		*((string *) target) = (const char *) value; 
	}
	else if(type == mslOPT_ARG___INT) {
		*((int *) target) = atoi((const char *) value); 
	}
}

/* ############################################################################################ */

void optCallback_Usage(void *aa, void *arg_val)
{
	printUsage();
	exit(0);
}

void printUsage() 
{
	printf("\n\n");
	printf("Usage: mslPronEval.test [options]\n");
	printf("       -h\n");
	printf("               help\n");
	printf("       -host string\n");
	printf("               server address (default: '%s') \n", DEFAULT_HOST);
	printf("       -f string\n");
	printf("               audio file path (default: '%s')\n", DEFAULT_FILENAME);
	printf("       -a string\n");
	printf("               answer_text (default: '%s')\n", DEFAULT_ANSWER_TEXT);
	printf("\n\n");
	 
}
