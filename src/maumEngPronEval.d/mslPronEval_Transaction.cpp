#include <stdio.h>
#include "mslPronEval_Transaction.h"


Transaction::Transaction(int max) : list_data(max)
{
	/* 빈 공간을 빠르게 검색하기 위해 빈 노드 링크를 등록한다. */
	for(int xx = 0; xx < max; xx++) {
		link_empty.push_back(xx);
	}
}

Transaction::~Transaction()
{
	for(int xx = 0; xx < list_data.size(); xx++) {
		if(list_data[xx] == NULL) continue;
		if(list_data[xx]->thr.joinable() == false) continue;

		list_data[xx]->thr.join();
	}
}

/*
 * @desc		빈 노드에 노드 등록
 * @return		succ > 등록된 노드 인덱스
 * 				fail > -1
 */
int Transaction::set(GrpcEvalClient *node)
{
	int index = -1;

	/* LOCK - 자동 락해제 됨*/
	unique_lock<mutex> auto_lock(lock);

	/* 여유분이 없으면 에러 리턴 */
	if(link_empty.size() == 0) return index;

	/* 빈 노드 인덱스 할당 */
	index = link_empty.back();
	link_empty.pop_back();
	list_data[index] = node;

	return index;
}

GrpcEvalClient *Transaction::get(int idx)
{
	/* LOCK - 자동 락해제 됨 */
	unique_lock<mutex> auto_lock(lock);

	/* 범위 밖이면 바로 리턴 */
	if(idx < 0 || idx >= list_data.size()) return NULL;

	return list_data[idx];	
}

/*
 * @desc		지정 노드 해지
 */
void Transaction::reset(int index)
{
	/* LOCK - 자동 락해제 됨 */
	unique_lock<mutex> auto_lock(lock);

	/* 범위 밖이면 바로 리턴 */
	if(index < 0 || index >= list_data.size()) return; 

	/* 비어 있는 슬롯이면 바로 리턴 */
	if(list_data[index] == NULL) return;

	/* 리셋 */
	delete list_data[index];
	list_data[index] = NULL;

	/* 빈노드 링크에 등록 */
	link_empty.push_back(index);
}


