#ifndef __mslPronEval_Log_h__
#define __mslPronEval_Log_h__

#include <spdlog2/spdlog.h>

/* ################################################################################## */

#define LOGD(...)      {g_log->debug(__VA_ARGS__);    g_log->flush();}
#define LOGI(...)      {g_log->info(__VA_ARGS__);     g_log->flush();}
#define LOGW(...)      {g_log->warn(__VA_ARGS__);     g_log->flush();}
#define LOGE(...)      {g_log->error(__VA_ARGS__);    g_log->flush();}
#define LOGC(...)      {g_log->critical(__VA_ARGS__); g_log->flush();}

/* ################################################################################## */

extern std::shared_ptr<spdlog::logger>      g_log;

#endif
