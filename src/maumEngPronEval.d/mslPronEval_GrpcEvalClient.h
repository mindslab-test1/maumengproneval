#ifndef __mslPronEval_GrpcEvalClient_h__
#define __mslPronEval_GrpcEvalClient_h__

#include <iostream>
#include <memory>
#include <string>
#include <thread>

#include <grpcpp/grpcpp.h>
#include <grpc/support/log.h>

#include "mslPronEval.grpc.pb.h"
#include "mslPronEval_ResultCode.h"

using namespace std;

using grpc::Server;
using grpc::ServerAsyncResponseWriter;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::ServerCompletionQueue;
using grpc::Status;

using ai::mindslab::eduai::pron_eval::Request_EnglishPronEval;
using ai::mindslab::eduai::pron_eval::Response_EnglishPronEval;
using ai::mindslab::eduai::pron_eval::Service_EnglishPronEval;


class GrpcEvalClient {
public:

	GrpcEvalClient(Service_EnglishPronEval::AsyncService* service, ServerCompletionQueue* cq)
		: service_(service), cq_(cq), responder_(&ctx_), status_(CREATE) {

    	proceed();
    }
	~GrpcEvalClient();
      
	void proceed();
	void sendError(ResultCode code);

	ServerAsyncResponseWriter<Response_EnglishPronEval> *getResponder() { return &responder_; }

public:
	enum CallStatus { CREATE, PROCESS, FINISH };

	thread thr;
	int trans_idx = -1;		// TRANSACTION TABLE INDEX

	Request_EnglishPronEval request_;

private:

	Service_EnglishPronEval::AsyncService* service_;
	ServerCompletionQueue* cq_;
	ServerContext ctx_;


	ServerAsyncResponseWriter<Response_EnglishPronEval> responder_;

    CallStatus status_;
};


#endif
