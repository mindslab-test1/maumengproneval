#include <unistd.h>

#include "Laser.h"
#include "confmeasure.h"
#include "pronClinic.h"

#include "mslPronEval_Env.h"
#include "AppConfig.h"

Env::Env() : trans(g_conf->grpc.max_thread) 
{
	/* 발음 평가 엔진 설정 정보 로딩 */
	pron_struct = createPronScoreDnnMaster((char *) g_conf->pronlib.conf.c_str());
	if(pron_struct == NULL) {
		throw (string("Env >> pron-engine config fail - ") + g_conf->pronlib.conf).c_str();
	}
}

Env::~Env() 
{
	/* 발음 평가 엔진 설정 정보 자원 해지 */
	if(pron_struct != NULL) freePronScoreDnnMaster(pron_struct);
}
