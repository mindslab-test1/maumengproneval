#ifndef __mslPronEval_Transaction_h__
#define __mslPronEval_Transaction_h__

#include <iostream>
#include <vector>
#include <mutex>
#include <pthread.h>

#include "mslPronEval_GrpcEvalClient.h"

using namespace std;

/* ###################################################################################### */

class Transaction {
public:

	Transaction(int max);
	~Transaction();
	
	/* 빈 슬롯에 노드 등록, 등록된 인덱스 반환 */
	int set(GrpcEvalClient *);

	/* 등록된 노드 리셋 */
	void reset(int index);

	/* 등록 노드 반화 */
	GrpcEvalClient *get(int idx);

private:
	mutable mutex lock;
	vector<GrpcEvalClient *> list_data;
	vector<int>	link_empty;
};

#endif
