#ifndef __mslPronEval_Server_h__
#define __mslPronEval_Server_h__

#include <iostream>
#include <memory>
#include <string>
#include <pthread.h>

#include <grpcpp/grpcpp.h>
#include <grpc/support/log.h>

#include "mslPronEval.grpc.pb.h"
#include "mslPronEval_Transaction.h"

using namespace std;

using grpc::Server;
using grpc::ServerAsyncResponseWriter;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::ServerCompletionQueue;
using grpc::Status;

using ai::mindslab::eduai::pron_eval::Request_EnglishPronEval;
using ai::mindslab::eduai::pron_eval::Response_EnglishPronEval;
using ai::mindslab::eduai::pron_eval::Service_EnglishPronEval;

/* ########################################################################## */

class mslPronEval_Server {
public:
	mslPronEval_Server() {} 
	~mslPronEval_Server();

	void run();

private:
	void handleRpcs();

private:
	std::unique_ptr<ServerCompletionQueue> cq_;
  	Service_EnglishPronEval::AsyncService service_;
  	std::unique_ptr<Server> server_;

	thread thr;
};

#endif

