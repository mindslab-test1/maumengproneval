#include <sys/time.h>
#include <unistd.h>

#include "mslPronEval_GrpcEvalClient.h"
#include "mslPronEval_Env.h"
#include "mslPronEval_Log.h"
#include "mslPronEval_ResultCode.h"

/* --------------------------------------------------------------------------------------- */

#include "Laser.h"
#include "confmeasure.h"
#include "pronClinic.h"

#define MAX_LEN_SENTENCE		1024


/* ####################################################################################### */

int procAsrSvc(Response_EnglishPronEval *reply, std::string filename, std::string utterance);
void parsingClinicResult(Response_EnglishPronEval *reply, char* result_clinic);
int stringTokenize( char *str, char** wordA, char t_mask);
void setResult(Response_EnglishPronEval *reply, DECODER_RESULT* result, char *result_clinic);

/* ####################################################################################### */

void execEvaluationThread(int trans_idx) 
{
	int 						rcode = 0;
	GrpcEvalClient *			client = g_env->trans.get(trans_idx);
	Response_EnglishPronEval 	reply_;
	struct timeval				begin_time;
	struct timeval				end_time;
	

	gettimeofday(&begin_time, NULL);

	g_log->info("--> RECEIVED MESSAGE");
	g_log->info("    * trans_id = {}", client->request_.trans_id());
	g_log->info("    * filename = {}", client->request_.filename());
	g_log->info("    * utterance= {}", client->request_.utterance());
	g_log->info("    --------------------------------------------------------------------");

	reply_.set_trans_id(client->request_.trans_id());
	if(client == NULL) {
		g_log->error("    * result_code = {} >> internal error >> not found in trans table\n");
		reply_.set_result_code(static_cast<int>(ResultCode::fail));
	}
	else {
		rcode = procAsrSvc(&reply_, client->request_.filename(), client->request_.utterance());
		reply_.set_result_code(rcode);

		if(rcode == 0) {
			g_log->info("    * pron_score = {}", reply_.pron_score());
			g_log->info("    * holistic = {}", reply_.regression_holistic());
			g_log->info("    * speed = {}", reply_.regression_speed());
			g_log->info("    * rhythm = {}", reply_.regression_rhythm());
			g_log->info("    * intonation = {}", reply_.regression_intonation());
			g_log->info("    * segmental = {}", reply_.regression_segmental());
		}

		g_log->info("    * result_code = {}", reply_.result_code());
	}

	client->getResponder()->Finish(reply_, Status::OK, client);

	gettimeofday(&end_time, NULL);
	long dur = (end_time.tv_sec%1000*1000 + end_time.tv_usec/1000) - (begin_time.tv_sec%1000*1000 + begin_time.tv_usec/1000);
	g_log->info("    * duration = {:.3f}s\n", dur/1000.0);

	g_log->flush();
}

int procAsrSvc(Response_EnglishPronEval *reply, std::string filename, std::string utterance)
{
	void    *pronStructM = NULL;
    void    *pronStructC = NULL;
    char    *pronResult = NULL;
    DECODER_RESULT *result = NULL;


	int		rcode = 0;

	g_log->debug("step........ 1");
    pronResult = (char*)malloc(sizeof(char)*MAX_LEN_SENTENCE*12);
	if(pronResult == NULL) {
		LOGE("### OUT OF MEMORY >> procAsrSvc()\n");
		rcode = static_cast<int>(ResultCode::out_of_mem);
		goto __END;
	}
    strcpy(pronResult,"");

	g_log->debug("step........ 2");
    result = (DECODER_RESULT*) malloc( sizeof( DECODER_RESULT ) );
	if(result == NULL) {
		LOGE("### OUT OF MEMORY >> procAsrSvc()\n");
		rcode = static_cast<int>(ResultCode::out_of_mem);
		goto __END;
	}
    memset(result,0,sizeof(DECODER_RESULT));
    strcpy(result->m_result, utterance.c_str());
	result->m_confidence = 1;


	g_log->debug("step........ 4, text = {}", result->m_result);
    pronStructM = (void*) g_env->pron_struct;;

    pronStructC = createPronScoreDnnChild( pronStructM );

	g_log->debug("step........ 5, {}", filename.c_str());
    doPronScore( pronStructC, result, (char *) filename.c_str(), pronResult);    // teset file : sample.pcm

	g_log->debug("step........ 6, {}", pronResult);
    setResult(reply, result, pronResult);

	g_log->debug("step........ 7");
__END:
    if(pronStructC != NULL) freePronScore( pronStructC );
	g_log->debug("step........ 8");
    if(pronResult != NULL) free( pronResult );
	g_log->debug("step........ 9");
    if(result != NULL) free( result );
	g_log->debug("step........ 10");

	return rcode;
}

void setResult(Response_EnglishPronEval *reply, DECODER_RESULT* result, char *result_clinic)
{
	int xx;
	char **ctmpA;
    char *result_clinic_parsing;

	/* >> 문장 등록 */
	reply->set_sentence(result->m_result);

	/* >> uvScore 등록 */
	reply->set_confidence(result->m_confidence);

	/* >> pronScore 등록 */
	reply->set_pron_score(result->m_pronscore);

	/* >> stress_score 등록 */
	reply->set_stress_score(result->m_stressscore);

	/* 워드 저장 공간 확보 */
	// ctmpA = (char**)malloc(sizeof(char*) * MAX_NUM_WORD);
    // for(xx = 0; xx < MAX_NUM_WORD; xx++) ctmpA[xx] = (char*) malloc(sizeof(char)*MAX_LEN_WORDSTRING);

	/* 워드 설정  */
    for(xx = 0; xx < result->m_numword; xx++) {
        if(xx == 0) {
            strcpy(result->m_wordnameA[xx], "<s>");
        }
        else if(xx == result->m_numword-1) {
            strcpy(result->m_wordnameA[xx], "</s>");
        }
        else {
            // strcpy(result->m_wordnameA[xx], ctmpA[xx-1]);
			printf("word: %s > %d > %d > %f > %d > %d \n", result->m_wordnameA[xx],
															result->m_startA[xx],
															result->m_endA[xx],
															result->m_confidenceA[xx],
															result->m_pronscoreA[xx],
															result->m_stressA[xx]
					);

			Response_EnglishPronEval::Word *word = reply->add_list_word();
			word->set_name(result->m_wordnameA[xx]);
			word->set_start_time(result->m_startA[xx]);
			word->set_end_time(result->m_endA[xx]);
			word->set_confidence(result->m_confidenceA[xx]); 
			word->set_pron_score(result->m_pronscoreA[xx]); 
			word->set_stress(result->m_stressA[xx]);
        }
    }

/*
	Response_EnglishPronEval::Word *word = reply->mutable_list_word(2);
	g_log->debug("step........ e1");
	g_log->flush();

	printf("==============>: %s > %f > %d > %d \n"
			, word->name().c_str()
			, word->uv_score()
			, word->pron_score()
			, word->stress()
	);
*/


	/* Phonics 설정 */
    for(xx = 0; xx < result->m_numph; xx++) {
		printf("phonics: %s %d %d\n", result->m_phnameA[xx],
                                      result->m_phendA[xx],
                                      result->m_phscoreA[xx]);
		Response_EnglishPronEval::Phonics *phonics= reply->add_list_phonics();
		phonics->set_name(result->m_phnameA[xx]);
		phonics->set_end_time(result->m_phendA[xx]);
		phonics->set_score(result->m_phscoreA[xx]);
	}


	g_log->info("------------------------> pronscore = {}", result->m_pronscore);
	// >> Clinic 정보 설정 	
	if(result->m_pronscore != 0) parsingClinicResult(reply, result_clinic);
}

/* 워드 분리 */
int stringTokenize( char *str, char** wordA, char t_mask)
{
    char    *ptr;
    int     n_tok = 0;

    while( *str) {
        if( *str == '\0') break;

        ptr = strchr( str, t_mask);
        if( ptr ) {
            unsigned int len = ptr - str;
            strncpy( wordA[n_tok], str, len );
            wordA[n_tok][len] = '\0';
            n_tok ++;
        }
        else {
            strcpy( wordA[n_tok], str);
            n_tok ++;
            break;
        }

        str = ptr + 1;
    }

    return n_tok;
}

/* 각 평가 점수 분리 */
void parsingClinicResult(Response_EnglishPronEval *reply, char* result_clinic)
{
    char *ptr = result_clinic;
	float score;

g_log->debug("step... 1 >> {}", ptr); g_log->flush();
    ptr = strstr(ptr,"GENIE_HOLISTIC_ALL10k_KSEC");
g_log->debug("step... 1.. 1 / ptr={}", ptr); g_log->flush();
    ptr = strstr(ptr,"<evalScore>");
g_log->debug("step... 1.. 2"); g_log->flush();
	sscanf(ptr + strlen("<evalScore>"), "%f", &score);
g_log->debug("step... 1.. 3"); g_log->flush();
	// printf("--> HOLISTIC: %f\n", score);
	reply->set_regression_holistic(score);
	
g_log->debug("step... 2"); g_log->flush();
    ptr = strstr(ptr, "GENIE_SPEED_ALL10k_KSEC");
    ptr = strstr(ptr,"<evalScore>");
	sscanf(ptr + strlen("<evalScore>"), "%f", &score);
	// printf("--> SPEED: %f\n", score);
	reply->set_regression_speed(score);

g_log->debug("step... 3"); g_log->flush();
    ptr = strstr(ptr, "GENIE_RHYTHM_ALL10k_KSEC");
    ptr = strstr(ptr,"<evalScore>");
	sscanf(ptr + strlen("<evalScore>"), "%f", &score);
	// printf("--> RHYTHM: %f\n", score);
	reply->set_regression_rhythm(score);

g_log->debug("step... 4"); g_log->flush();
    ptr = strstr(ptr, "GENIE_INTONATION_ALL10k_KSEC");
    ptr = strstr(ptr,"<evalScore>");
	sscanf(ptr + strlen("<evalScore>"), "%f", &score);
	// printf("--> INTONATION: %f\n", score);
	reply->set_regression_intonation(score);

g_log->debug("step... 5"); g_log->flush();
    ptr = strstr(ptr, "GENIE_SEGMENTAL_ALL10k_KSEC");
    ptr = strstr(ptr,"<evalScore>");
	sscanf(ptr + strlen("<evalScore>"), "%f", &score);
	// printf("--> SEGMENTAL: %f\n", score);
	reply->set_regression_segmental(score);

g_log->debug("step... 6"); g_log->flush();
    ptr = strstr(ptr, "GENIE_SEGMENTAL_ALL10k_KSEC_FEAT19");
    ptr = strstr(ptr,"<evalScore>");
	sscanf(ptr + strlen("<evalScore>"), "%f", &score);
	// printf("--> SEGMENTAL_FEAT19: %f\n", score);
	reply->set_regression_segmental_feat19(score);
g_log->debug("step... 7"); g_log->flush();
}

