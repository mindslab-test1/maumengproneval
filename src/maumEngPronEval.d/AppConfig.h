#ifndef __AppConfig_h__
#define __AppConfig_h__

#include <iostream>
#include <string>
#include <libconfig.h++>

using namespace std;
using namespace libconfig;

/* ########################################################################## */

typedef struct {
	string			module;			// 모듈명
	string			level;
	string			mode;
} log_t;

typedef struct {
	int				port;
	int				max_thread;
} grpc_t;

typedef struct {
	string			conf;			// 평가 엔진 설정 파일 경로
} pronlib_t;

/* ########################################################################## */

class AppConfig {
public:
	AppConfig(string cfgpath);		

	log_t			log;
	grpc_t			grpc;
	pronlib_t		pronlib;

private:

	void parseLog(Setting &root);
	void parseGrpc(Setting &root);
	void parsePronLib(Setting &root);
	
};

/* ########################################################################## */

extern AppConfig *g_conf;


#endif
