#include "mslPronEval_Log.h"
#include "mslPronEval_GrpcEvalClient.h"
#include "mslPronEval_Evaluate.h"
#include "mslPronEval_Env.h"
#include "mslPronEval_ResultCode.h"

void GrpcEvalClient::proceed() 
{
	if (status_ == CREATE) {
		g_log->info("# rpcEvalClient::proceed() >> CREATE");

        // Make this instance progress to the PROCESS state.
        status_ = PROCESS;
        
		// 요청 대기 등록 
        service_->Request_evaluateEnglishPron(&ctx_, &request_, &responder_, cq_, cq_, this);
    } else if (status_ == PROCESS) {
		g_log->info("# rpcEvalClient::proceed() >> PROCESS");

		trans_idx = g_env->trans.set(this);
		/* 추가 자원 없음: 에러 반환 */
		if(trans_idx < 0) {
			sendError(ResultCode::queue_full);	
		}
		else {
			thr = move(thread(execEvaluationThread, trans_idx));
		}
        status_ = FINISH;
        
		/* FOR NEXT REQUEST */
        new GrpcEvalClient(service_, cq_);
    } else {
		g_log->info("# rpcEvalClient::proceed() >> status = {}", status_);

        GPR_ASSERT(status_ == FINISH);

		if(g_env->running_flag == true) g_env->trans.reset(trans_idx);
    }
}

GrpcEvalClient::~GrpcEvalClient() 
{
	if(thr.joinable()) thr.join();
}

/* 에러 반환 */
void GrpcEvalClient::sendError(ResultCode code)
{
	Response_EnglishPronEval reply_;

    reply_.set_result_code(static_cast<int>(code));
    getResponder()->Finish(reply_, Status::OK, this);
}
