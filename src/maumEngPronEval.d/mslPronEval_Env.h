#ifndef __mslPronEval_Env_h__
#define __mslPronEval_Env_h__

#include "AppConfig.h"
#include "mslPronEval_Transaction.h"

class Env {
public:
	Env();
	~Env();
	
	Transaction 		trans;											// GRPC 트랜젝션 관리
	bool 				running_flag = true;							// 기동중인지, 종료 중인지 설정
	void *				pron_struct = NULL;								// PRON 엔진을 위한 환경 로딩 정보
private:
};

/* ############################################################################################# */

extern Env *g_env;

#endif
