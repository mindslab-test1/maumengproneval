#ifndef __mslPronEval_ResultCode_h__
#define __mslPronEval_ResultCode_h__

enum class ResultCode {
	succ = 0,
	fail = -1,
	queue_full = -2,
	not_found = -3,
	out_of_mem = -4,
};


#endif
